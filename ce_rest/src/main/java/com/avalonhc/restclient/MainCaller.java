package com.avalonhc.restclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.drools.core.command.impl.CommandBasedStatefulKnowledgeSession;
import org.drools.core.command.runtime.rule.InsertObjectCommand;
import org.drools.core.rule.constraint.ConditionAnalyzer.ThisInvocation;
import org.jbpm.process.audit.ProcessInstanceLog;
import org.kie.api.runtime.KieSession;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.Task;
import org.kie.api.task.model.TaskSummary;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.remote.jaxb.gen.FindVariableInstancesCommand;
import org.kie.remote.jaxb.gen.FireAllRulesCommand;
import org.kie.services.client.api.RemoteRuntimeEngineFactory;
import org.kie.services.client.api.command.RemoteRuntimeEngine;
import org.kie.services.client.serialization.jaxb.impl.audit.JaxbVariableInstanceLog;
import org.kie.services.client.serialization.jaxb.impl.task.JaxbTaskSummary;

import com.avalonhc.as.facts.ClaimHistFreqDosUnit;
import com.avalonhc.as.facts.ClaimHistoryUnitHelper;
import com.avalonhc.as.facts.ClaimRequest;
import com.avalonhc.as.facts.DateUtility;
import com.avalonhc.as.facts.EvaluateLabClaim;
import com.avalonhc.as.facts.EvaluateLabClaimResponse;
import com.avalonhc.as.facts.ResponseHeaderData;
import com.avalonhc.as.facts.ResponseLineData;
import com.avalonhc.as.facts.SecondaryCodes;


import org.kie.api.task.model.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SuppressWarnings("deprecation")
public class MainCaller {

	
	static String user="krisv";
	static String password = "krisv";
	static String deploymentId = "org.jboss.as.facts:Claim:2.4.2-beta";
	static String processId = "defaultPackage.GlobalFlow";
//	static String processId="ClaimEditProcess.WholesaleExclusionsSubProcess";
	static String applicationUrlString = "http://localhost:8080/jbpm-console/";
	static String processUrl ="http://localhost:8080/jbpm-console/runtime/"+deploymentId+"/process/"+processId +"/start";
	
	@SuppressWarnings("unused")
	
	
// Amit -- this will be the method which will accept EvaluateLabClaim Object as input	
	public static void main(String[] args) throws ParseException, DatatypeConfigurationException {
		// TODO Auto-generated method stub
//.addExtraJaxbClasses(ClaimHistFreqDosUnit.class, ClaimHistoryUnitHelper.class, ClaimRequest.class, EvaluateLabClaimResponse.class,SecondaryCodes.class, ResponseHeaderData.class, ResponseLineData.class, EvaluateLabClaim.class);		
		RuntimeEngine engine =null;
		try {
			 engine = RemoteRuntimeEngineFactory.newRestBuilder()
				    .addUrl(new URL(applicationUrlString))
				    .addUserName(user).addPassword(password)
				    .addDeploymentId(deploymentId)
				    .addExtraJaxbClasses(ClaimHistFreqDosUnit.class,ClaimHistoryUnitHelper.class,ClaimRequest.class,EvaluateLabClaimResponse.class,SecondaryCodes.class, ResponseHeaderData.class, ResponseLineData.class, EvaluateLabClaim.class )
				      .build();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MainCaller maincaller = new MainCaller();
		// Amit  since the method will get an evaluateLab clim this call (build eval) is not required
		EvaluateLabClaim eval = maincaller.buildEval();
		
		// Amit - this call will convert the input to a map for processing
		Map input = maincaller.getMapFromEvaluateClaimRequest(eval);
		 
		KieSession ksession = engine.getKieSession();
		ProcessInstance processInstance= ksession.startProcess(processId, input);
	   org.kie.api.runtime.manager.audit.ProcessInstanceLog log =engine.getAuditService().findProcessInstance(processInstance.getId());
	// ------------------------------ response header data ------------
 List varHeaderLog = engine.getAuditService().findVariableInstances(processInstance.getId(),"HeaderData");
	   String reqHeader="";
	   int lastHeader =  varHeaderLog.size();
	   if (lastHeader>0){
	   JaxbVariableInstanceLog lastObjHeader = (JaxbVariableInstanceLog) varHeaderLog.get(lastHeader-1);
	   reqHeader = lastObjHeader.getValue();
	   }
	   // Amit Naresh - since the thing is in Rest - we get a string back. Right now I saw only ResponseLine. I will have to add line for response
	   // header?
	   
	   
	   
	
	   
	   
	   
	   
	//------start response line data   
	   List varLog = engine.getAuditService().findVariableInstances(processInstance.getId(),"responseLineData");
	   
	   int last =  varLog.size();
	   JaxbVariableInstanceLog lastObj = (JaxbVariableInstanceLog) varLog.get(last-1);
	
	   // Amit Naresh - since the thing is in Rest - we get a string back. Right now I saw only ResponseLine. I will have to add line for response
	   // header?
	   
	   
	   String requestLine = lastObj.getValue();
	  // -- string response line data
	  
	  System.out.print(requestLine);
	
	// Amit - we will have to pars the requestHeaderLine and requestLine and create the evaluateClaim Object.

	}
	
	
	// AMit this method is not required
	EvaluateLabClaim buildEval() throws DatatypeConfigurationException{
		com.avalonhc.as.facts.EvaluateLabClaim eval = new com.avalonhc.as.facts.EvaluateLabClaim();
		com.avalonhc.as.facts.RequestHeaderData header = new com.avalonhc.as.facts.RequestHeaderData();
		header.setIdCardNumber("ZCL06440277");
		header.setHealthPlanId("401");
		header.setHealthPlanIdType("EI");
		header.setHealthPlanGroupId("031234547");
		header.setClaimNumber("10202");
		header.setHealthPlanMemberId("1123S");
		header.setPatientLastName("SQUAD");
		header.setPatientFirstName("SID");
		header.setIcdDiagVersQualifier("10");
		DatatypeFactory dtf = DatatypeFactory.newInstance();
		XMLGregorianCalendar dob = dtf.newXMLGregorianCalendarDate(1972, 05, 20,-6);
		XMLGregorianCalendar fds = dtf.newXMLGregorianCalendarDate(2015, 8, 06, -6);
		XMLGregorianCalendar tds = dtf.newXMLGregorianCalendarDate(2015, 8, 07, -6);
		header.setPatientDateOfBirth(dob);
		header.setPatientGenderCode("M");
		header.setPrimaryDiagnosisCode("A");
		ArrayList<String> diagnosisCodes = new ArrayList();
		diagnosisCodes.add("M810");
		diagnosisCodes.add("shshs");
		header.getDiagnosisCodes().addAll(diagnosisCodes);
		header.setBillingProviderTaxId("123456777");
		header.setBillingProviderNpi("123456777");
		header.setNumberOfLines(1);
	    header.setFromDateOfService(fds);
	    header.setToDateOfService(tds);
	    header.setLineOfBusiness("4");
		
	    com.avalonhc.as.facts.RequestLineData rds = new com.avalonhc.as.facts.RequestLineData();
		rds.setLineNumber(1);
		rds.setFromDateOfService(fds);
		rds.setToDateOfService(tds);
		rds.setPlaceOfService("81");
		rds.setProcedureCode("87480");
		//rds.getProcedureCodeModifiers().add("00");
		rds.setUnits(12);
		rds.getDiagnosisCodePointers().add("1");
		
		rds.setRenderingProviderNpi("1234567");
		rds.setInNetworkIndicator("Y");
		rds.setPaidDeniedIndicator("P");
		eval.setRequestHeader(header);
	    eval.getRequestLine().add(rds);	
	    return eval;
		
		
		
		
	}
	
	
	 
// Amit Naresh -- this method is required to convert the evaluateClaimObject into ClaimRequestObject 	 
	
static Map  getMapFromEvaluateClaimRequest(EvaluateLabClaim evaluateLabClaim) throws ParseException{
	  Map<String, Object> params = new HashMap<String, Object>();
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
     ClaimRequest claimRequest =new ClaimRequest();
     System.out.println("healthpland:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
     if(null!= evaluateLabClaim.getRequestHeader().getHealthPlanId() && !"".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId()) && !"?".equals(evaluateLabClaim.getRequestHeader().getHealthPlanId())){
 		claimRequest.setHealthPlanId(evaluateLabClaim.getRequestHeader().getHealthPlanId().trim());
 		System.out.println("Health plan:::"+evaluateLabClaim.getRequestHeader().getHealthPlanId());
 		}
 	
	        	
     if(null!=evaluateLabClaim.getRequestLine().get(0).getProcedureCode() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getProcedureCode())){
	 		   claimRequest.setProcedureCode(evaluateLabClaim.getRequestLine().get(0).getProcedureCode().trim());
	 		   System.out.println("procedure code:::"+ evaluateLabClaim.getRequestLine().get(0).getProcedureCode());
	 		} 
	         
     if(null!=evaluateLabClaim.getRequestLine().get(0).getFromDateOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService())){
	     
     	claimRequest.setFromDateOfService(sdf.parse(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService().toString().trim()));
    	 //claimRequest.setFromDateOfService(evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
	        	 System.out.println("From Date::"+evaluateLabClaim.getRequestLine().get(0).getFromDateOfService());
	         }
	      
	        	 System.out.println("DOB::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth() );
	        	 System.out.println("primaryDiagnosis:::"+evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode());
	        	 
	        	 if(null!=evaluateLabClaim.getRequestLine().get(0).getPlaceOfService() && !"".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService())){
	 	        	   claimRequest.setPlaceOfService(evaluateLabClaim.getRequestLine().get(0).getPlaceOfService().trim());
	    	        	 System.out.println("PlaceOfService::::"+evaluateLabClaim.getRequestLine().get(0).getPlaceOfService());
	 	        	  }
	        	 //setting PrimaryDiagnosisCode and DiagnosisCode in List for comparing with single column value in decision table
	        	 claimRequest.setError("NO");
	        	 List<String> diagnosisCodesList =new ArrayList<String>();
	        	 int DiagnosisCodePointersCount=0;
	        	
	        	 if(null== evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() || "".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()) || "?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
	        		 if (null != evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0) && !"".equals(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0))){ 
	   	        	  DiagnosisCodePointersCount =Integer.parseInt(evaluateLabClaim.getRequestLine().get(0).getDiagnosisCodePointers().get(0));
	        		 claimRequest.setDiagnosisCodePointers(DiagnosisCodePointersCount);
	        	 }
	        	 }
	        	 int count = evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size(); 
	        	 
	        	 System.out.println("Step 1 diagnosisCodesList ---->");
	        	 
	        	
	        		if(null!= evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode()) && !"?".equals(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode())){
	      	        	 diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
	      	        	 claimRequest.setPrimaryDiagnosisCode(evaluateLabClaim.getRequestHeader().getPrimaryDiagnosisCode().trim());
	      	        	}
	        		System.out.println("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	  	        		System.out.println("in for loop the value of count1"+count);
	  	        	if(null== evaluateLabClaim.getRequestHeader().getDiagnosisCodes() || "".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes()) || "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
	  	        		System.out.println("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	  	        		System.out.println("in for loop the value of count"+count);
	  	        		for (int i = 0; i < count; i++) {
	  	        			System.out.println("in for loop the value of i"+i);
	  	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
	  	        			System.out.println("in for loop diagnosiscode list"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
	  	      		}
	  	       
	  	        	 
	  	        	}
	        		 System.out.println("Step 1 diagnosisCodesList ---->");
	      	        	if(null!= evaluateLabClaim.getRequestHeader().getDiagnosisCodes() && !"".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())&& ! "?".equals(evaluateLabClaim.getRequestHeader().getDiagnosisCodes())){
	      	        	// diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes());
	      	        		for(int  i=0;i<evaluateLabClaim.getRequestHeader().getDiagnosisCodes().size();i++){
	      	        			System.out.println("diagnosisCodesList ---->"+evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i));
	      	        			diagnosisCodesList.add(evaluateLabClaim.getRequestHeader().getDiagnosisCodes().get(i).trim());
	      	        		}
	      	        		
	      	        	}
	      	        	System.out.println("diagnosisCodesList ---->"+diagnosisCodesList); 
	      	        	claimRequest.getDiagnosisCodesList().addAll(diagnosisCodesList);
	     	        	 
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientGenderCode() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getPatientGenderCode()) ){
	        	        	 System.out.println("Gender::::"+evaluateLabClaim.getRequestHeader().getPatientGenderCode());
	        	        	 claimRequest.setPatientGenderCode(evaluateLabClaim.getRequestHeader().getPatientGenderCode().trim());
	        	        	}
	     	        	
	     	        	
	     	        	if(null!=evaluateLabClaim.getRequestHeader().getPatientDateOfBirth() && !"".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()) && ! "?".equals(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth()) ){ 
	     	        		
	     	        		claimRequest.setPatientDOB((evaluateLabClaim.getRequestHeader().getPatientDateOfBirth().toString().trim()));
	     	        		 System.out.println("Patient DOB::::"+evaluateLabClaim.getRequestHeader().getPatientDateOfBirth());
	     	        	}
	     	        	
	     	        	if((evaluateLabClaim.getRequestLine().get(0).getUnits())!= 0 && !"".equals(evaluateLabClaim.getRequestLine().get(0).getUnits()) && !"?".equals(evaluateLabClaim.getRequestLine().get(0).getUnits())){
	 	 	        	   claimRequest.setUnits(evaluateLabClaim.getRequestLine().get(0).getUnits());
	 	    	        	 System.out.println("Units::::"+evaluateLabClaim.getRequestLine().get(0).getUnits());
	 	 	        	  }
	     	        	
	     	        	else
	     	        	{
	     	        		claimRequest.setPatientDOB(null);
	     	        	}
	     	        	
	     	        	
	     	         claimRequest.setIdCardNumber(evaluateLabClaim.getRequestHeader().getIdCardNumber()); 	
	     	         claimRequest.setEffectiveEndDate(sdf.parse(evaluateLabClaim.getRequestLine().get(0).getToDateOfService().toString()));
	     	         int calculatedAge = DateUtility.calculateAge(sdf.parse(evaluateLabClaim.getRequestHeader().getPatientDateOfBirth().toString()));	 	
                  claimRequest.setCalculatedAge(calculatedAge);
              //    claimRequest.setAssociatedMPolicy(evaluateLabClaim.getRequestHeader().get);
               //   claimRequest.setInNetworkIndicator(inNetworkIndicator);
 	        	//Sample Response creation for global flow
	     	        	UUID uuid = UUID.randomUUID();
	     			    String randomUUIDString = uuid.toString();
	     	        	
 	        	ResponseHeaderData responseHeaderData = new ResponseHeaderData();
 	        	
 	        	System.out.println("randomUUIDString : : : "+randomUUIDString);
 	        	responseHeaderData.setCeTransactionId(randomUUIDString);
 	        
 	        	responseHeaderData.setClaimNumber(evaluateLabClaim.getRequestHeader().getClaimNumber());
	        	System.out.println("ClaimNumber::::"+evaluateLabClaim.getRequestHeader().getClaimNumber());	 
	            	EvaluateLabClaimResponse evaluateLabClaimResponse = new EvaluateLabClaimResponse();
 	        	SecondaryCodes secondaryCodes = new SecondaryCodes();
 	        	ResponseLineData responseLineData =new ResponseLineData();
 	        	
 	        	
 	        	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
 	        	evaluateLabClaimResponse.getResponseLine().add(responseLineData);
 	      //  	evaluateLabClaimResponse.setResponseHeader(responseHeaderData);
 	        	
	        	 claimRequest.setPatientGenderCode("F");
	        	 ArrayList diagList = new ArrayList<String>();
	        	 diagList.add("N760");
	        	 diagList.add("N761");
	        	 claimRequest.setDiagnosisCodesList(diagList);
	        	 claimRequest.setBlueCardCode("1");
	        	 claimRequest.setInNetworkIndicator("Y");
	        	// claimRequest.isSerivceLineDiagonsisCodesIn(arg0, arg1)
	        	 params.put("claimReq", claimRequest);
	        	 params.put("evaluateLabClaimResponse", evaluateLabClaimResponse);
	        	 params.put("secondaryCodes", secondaryCodes);
	        	 params.put("responseLineData", responseLineData);
	  
	        	 	return params;
	  
	  
	  
	  
}
	

}
